<?php
/**
 * @file
 * Admin settings form.
 */

/**
 * Generate admin setting form.
 */
function ignitionone_admin_form() {
  $form = array();

  $form['ignitionone_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#default_value' => ignitionone_get_host(),
    '#size' => 255,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['ignitionone_id'] = array(
    '#type' => 'textfield',
    '#title' => t('ID'),
    '#default_value' => ignitionone_get_id(),
    '#size' => 100,
    '#maxlength' => 100,
    '#required' => TRUE,
  );

  $form['ignitionone_exclude_paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Exclude tracking on specified pages'),
    '#default_value' => ignitionone_get_exclude_paths(),
  );

  $role_options = array_map('check_plain', user_roles());
  $form['ignitionone_exclude_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Exclude tracking for specified roles'),
    '#default_value' => ignitionone_get_exclude_roles(),
    '#options' => $role_options,
    '#description' => t('If none of the roles are selected, all users will be tracked.'),
  );

  return system_settings_form($form);
}
